# Validate labels on issues.

import config
import utils

def get_repo_labels(issue_url):
    repo_url = "/".join(issue_url.split("/")[:-2])
    return utils.gitea_api_get(repo_url + "/labels")

def issues_event(data):
    repo = data["repository"]["full_name"]
    if repo not in config.GITEA_REPOS:
        return
    if data.get("is_pull", False):
        return

    action = data.get("action", "")
    commit_id = data.get("commit_id", "")

    issue = data["issue"]
    issue_url = issue["url"]
    issue_state = issue["state"]
    issue_labels = issue["labels"]

    # Ensure valid status on issue close/open.
    status_closed = {"Status/Archived", "Status/Resolved", "Status/Duplicate"};
    status_prefix = "Status/"
    type_prefix = "Type/"

    # Determine new status to set, if any.
    if action == "closed":
        if commit_id == "":
            for label in issue_labels:
                if label["name"] in status_closed:
                    return
            new_status = "Status/Archived"
        else:
            new_status = "Status/Resolved"
    elif action == "reopened":
        for label in issue_labels:
            if label["name"].startswith(status_prefix) and \
               label["name"] not in status_closed:
                return

        # Set needs triage or confirmed depending if report was classified.
        new_status = "Status/Needs Triage"
        for label in issue_labels:
            if label["name"].startswith(type_prefix) and \
               label["name"] != "Type/Report":
                new_status = "Status/Confirmed"
    else:
        return

    repo_labels = get_repo_labels(issue_url)

    for label in repo_labels:
        if label["name"] == new_status:
            new_status_id = label["id"]

    # Replace previous status with new one.
    new_labels = [new_status_id]
    old_labels = []
    for label in issue_labels:
        old_labels.append(label["id"])
        if not label["name"].startswith(status_prefix):
            new_labels.append(label["id"])

    if sorted(new_labels) != sorted(old_labels):
        utils.gitea_api_put(issue_url + "/labels", {"labels": new_labels})
