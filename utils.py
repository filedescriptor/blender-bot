
import config

import json
import os
import requests
import subprocess

from typing import Sequence, Optional

# Logging
def log(msg: str):
    print(msg)

# Commands
def call(cmd: Sequence[str], silence_error: bool=False) -> Optional[str]:
    try:
        output = subprocess.check_output(cmd, stderr=subprocess.STDOUT, universal_newlines=True)
    except subprocess.CalledProcessError as e:
        if silence_error:
            return ""
        else:
            log(str(e) + "\n" + e.output)
            raise e
    return output.strip()

# Git
def git_call(cmd: Sequence[str], repo: str, silence_error: bool=False) -> Optional[str]:
    repo_dir = config.REPOS_DIR / (repo + ".git")
    git_cmd = ["git", "--git-dir", repo_dir] + cmd

    return call(git_cmd, silence_error=silence_error)

def git_fetch_notes(repo: str):
    git_call(["fetch", "origin"], repo)

def git_push_notes(repo: str):
    git_call(["push", "-f", "origin", "refs/notes/*"], repo)

def git_get_note(repo: str, commit: str) -> str:
    return git_call(["notes", "show", commit], repo, silence_error=True)

def git_set_note(repo: str, commit: str, note: str):
    git_call(["notes", "add", "-f", "-m", note, commit], repo)

def git_remove_all_notes(repo: str):
    git_call(["update-ref", "-d", "refs/notes/commits"], repo)

def git_is_commit(repo: str, commit: str) -> bool:
    return git_call(["cat-file", "-t", commit], repo, silence_error=True) == "commit"

def git_get_commit_message(repo: str, commit: str) -> str:
    return git_call(["log", "--format=%B", "-n", "1", commit], repo)

def git_fetch_all_commits_in_branch(repo: str, branch: str) -> Sequence[str]:
    return git_call(["log", "--pretty=format:%h", branch], repo).strip().split("\n")

# Gitea
def gitea_private_url(url):
    if url.startswith(config.GITEA_BASE_PUBLIC_URL):
        return url.replace(config.GITEA_BASE_PUBLIC_URL, config.GITEA_BASE_URL, 1)
    return url

def gitea_public_url(url):
    if url.startswith(config.GITEA_BASE_URL):
        return url.replace(config.GITEA_BASE_URL, config.GITEA_BASE_PUBLIC_URL, 1)
    return url

def gitea_api_get(url, data=None):
    headers = {"accept": "application/json",
               "Authorization": "token " + config.GITEA_API_TOKEN}
    if data:
        response = requests.get(gitea_private_url(url), data=json.dumps(data), headers=headers)
    else:
        response = requests.get(gitea_private_url(url), headers=headers)
    if not response.ok:
        log(response.text)
        raise Exception("Failed to get " + url)
    return response.json()

def gitea_api_get_total_count(url, data=None):
    headers = {"accept": "application/json",
               "Authorization": "token " + config.GITEA_API_TOKEN}
    if data:
        response = requests.get(gitea_private_url(url), data=json.dumps(data), headers=headers)
    else:
        response = requests.get(gitea_private_url(url), headers=headers)
    if not response.ok:
        log(response.text)
        raise Exception("Failed to get " + url)
    return int(response.headers['x-total-count'])

def gitea_api_put(url, data):
    headers = {"accept": "application/json",
               "Content-Type": "application/json",
               "Authorization": "token " + config.GITEA_API_TOKEN}
    response = requests.put(gitea_private_url(url), data=json.dumps(data), headers=headers)
    if not response.ok:
        log(response.text)
        raise Exception("Failed to put " + url)
    return response.json()

def gitea_api_post(url, data):
    headers = {"accept": "application/json",
               "Content-Type": "application/json",
               "Authorization": "token " + config.GITEA_API_TOKEN}
    response = requests.post(gitea_private_url(url), data=json.dumps(data), headers=headers)
    if not response.ok:
        log(response.text)
        raise Exception("Failed to post " + url)
    return response.json()

# Buildbot
def buildbot_enabled():
    return config.BUILDBOT_PASSWORD != None

def buildbot_api_post(url, data):
    headers = {"accept": "application/json",
               "Content-Type": "application/json"}

    session = requests.Session()
    auth_url = config.BUILDBOT_BASE_URL + "/auth/login"
    response = session.get(auth_url, auth=(config.BUILDBOT_USERNAME, config.BUILDBOT_PASSWORD))
    if not response.ok:
        log(response.text)
        raise Exception("Failed to authenticate on " + auth_url)
    response = session.post(url, data=json.dumps(data), headers=headers)
    if not response.ok:
        log(response.text)
        raise Exception("Failed to post " + url)
    return response.json()
