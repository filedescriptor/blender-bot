# Trigger buildbot.

import config
import utils
from typing import Sequence, Dict, Set, Any

def post_comment(reply_url: str, body: str):
    """
    Post comment to issue.
    """
    if reply_url != None:
        utils.gitea_api_post(reply_url + "/comments", {'body': body})


def get_team_can_commit(org: str, repo: str, team: Dict[str, Any]) -> bool:
    """
    Test if a team can commit to the given repository.
    """
    if "repo.code" not in team["units"]:
        return False
    if team["permission"] == "owner":
        return False
    if team["units_map"]["repo.code"] != "write":
        return False
    if team["includes_all_repositories"]:
        return True

    team_id = team["id"]
    repositories = utils.gitea_api_get(config.GITEA_API_URL + f"/teams/{team_id}/repos")

    for repository in repositories:
        if repository["full_name"] == f"{org}/{repo}":
            return True

    return False


def get_repo_committers(full_repo: str) -> Set[str]:
    """
    Get list of usernames with commit access to the given repo.
    """
    org, repo = full_repo.split('/', 1)

    # Get teams in repository.
    teams = utils.gitea_api_get(config.GITEA_API_URL + f"/orgs/{org}/teams")

    committers = set()
    for team in teams:
        if not get_team_can_commit(org, repo, team):
            continue

        team_id = team["id"]
        team_members_url = config.GITEA_API_URL + f"/teams/{team_id}/members"

        # Have to do pagination because there is a limit to limit.
        # Page indices are 1 based.
        num = utils.gitea_api_get_total_count(team_members_url)
        limit = 25
        num_pages = (num + limit - 1) // limit

        for page in range(0, num_pages):
            team_members = utils.gitea_api_get(team_members_url + f"?limit={limit}&page={page}")
            for member in team_members:
                if member["login"] == "blender-bot":
                    # Force disable automated bot account from committers.
                    continue
                committers.add(member["username"])

    return committers


def user_can_build(reply_url: str, repo: str, username: str) -> bool:
    """
    Test if given username is allow to build the given repository.
    """
    try:
        can_build = username in get_repo_committers(repo)
    except Exception as err:
        post_comment(reply_url, "Failed to verify commit access. " + config.INFO_MESSAGE)
        raise err

    return can_build


def get_platforms(reply_url: str, platform_args: Sequence[str]) -> Sequence[str]:
    """
    Get plaforms to build for.
    """
    default_platforms = ["darwin-x86_64", "darwin-arm64", "linux-x86_64", "windows-amd64"]

    if len(platform_args) == 0:
        return default_platforms

    platforms = set()

    for arg in platform_args:
        arg = arg.lower().replace("macos", "darwin")
        found = False

        for default_platform in default_platforms:
            if arg in {default_platform, default_platform.split("-")[0]}:
                platforms.add(default_platform)
                found = True

        if not found:
            post_comment(reply_url, f"Unknown platform `{arg}`. " + config.INFO_MESSAGE)
            return []

    return list(platforms)


def get_pull_commit_hash(repo: str, pull_id: str):
    """
    Get commit hash from pull request. Not provided as part of comment
    event but for pull request action we could read it directly.
    """
    result = utils.gitea_api_get(config.GITEA_API_URL + f"/repos/{repo}/pulls/{pull_id}")
    return result['head']['sha']


def build(repo: str, reply_url: str, html_url: str, build_type: str, build_id: str, username: str, package: bool, platform_args: Sequence[str]):
    """
    Build blender on the buildbot.
    """
    # Test if we can build.
    if repo == "blender/blender":
        coordinator_prefix = "vexp-code"

        platforms = get_platforms(reply_url, platform_args)
        if len(platforms) == 0:
            return

        params = {"needs_package_delivery": package,
                  "platform_architectures": platforms,
                  "build_configuration": "release"}

    elif repo == "blender/blender-manual":
        coordinator_prefix = "vexp-doc-manual"

        if package:
            post_comment(reply_url, "Package not supported for blender/blender-manual. " + config.INFO_MESSAGE)
            return
    else:
        post_comment(reply_url, "Can only build blender/blender and blender/blender-manual. " + config.INFO_MESSAGE)
        return

    if not utils.buildbot_enabled():
        post_comment(reply_url, "Buildbot not enabled in blender-bot. " + config.INFO_MESSAGE)
        return

    if not user_can_build(reply_url, repo, username):
        post_comment(reply_url, "Only blender organization members with write access can start builds. " + config.INFO_MESSAGE)
        return

    # Invoke buildbot API.
    reason = "blender-bot from " + utils.gitea_public_url(html_url)
    params.update({build_type: build_id, "reason": reason})

    data = {"method": "force",
            "jsonrpc": "2.0",
            "id": 0,
            "params" : params}

    if build_type == "patch_id":
        try:
            data["params"]["pull_revision"] = get_pull_commit_hash(repo, build_id)
        except Exception as err:
            post_comment(reply_url, "Failed to get commit hash for pull request.")
            raise err

        url = config.BUILDBOT_BASE_URL + f"/api/v2/forceschedulers/{coordinator_prefix}-patch-coordinator-force"
    else:
        url = config.BUILDBOT_BASE_URL + f"/api/v2/forceschedulers/{coordinator_prefix}-experimental-coordinator-force"

    try:
        utils.buildbot_api_post(url, data)
    except Exception as err:
        post_comment(reply_url, "Failed to start build, can't connect to buildbot.")
        raise err

    # Report back in comment.
    if package:
        if build_type == "patch_id":
            download_url = config.BUILDBOT_DOWNLOAD_URL + "/patch/PR" + build_id
        else:
            download_url = config.BUILDBOT_DOWNLOAD_URL + "/experimental/" + build_id

        body = f"Package build started. [Download here]({download_url}) when ready."
        post_comment(reply_url, body)
    elif build_type != "patch_id":
        # Hoping pull request status indicator is quick enough to not need this noise.
        post_comment(reply_url, "Branch build started.")


def unknown_command(reply_url: str, command: str):
    """
    Report unknown command.
    """
    if len(command) > 64:
        command = command[:64] + "..."
    body = f"Unknown command `{command}`. " + config.INFO_MESSAGE
    post_comment(reply_url, body)


def comment_event(data: Dict[str, Any]):
    """
    Handle commands in comments of issues and pull requests.
    """
    repo = data["repository"]["full_name"]
    if repo not in config.GITEA_REPOS:
        return

    action = data.get("action", "")
    if action != "created":
        return
    is_pull = ("pull_request" in data) or data.get("is_pull", False)

    comment = data["comment"]
    body = comment["body"]
    username = comment["user"]["username"]
    issue_number = str(data['issue']['number'])
    reply_url = data['issue']['url']
    html_url = data['issue']['html_url']

    prefix = "@blender-bot "

    lines = body.split("\n")
    for line in lines:
        line = line.strip()
        if not line.startswith(prefix):
            continue

        tokens = line[len(prefix):].strip().split()
        command = tokens[0]
        args = tokens[1:]
        if command == "build" and is_pull:
            build(repo, reply_url, html_url, 'patch_id', issue_number, username, False, args)
        elif command == "build-branch" and len(args) >= 1:
            build(repo, reply_url, html_url, 'override_branch_id', args[0], username, False, args[1:])
        elif command == "package" and is_pull:
            build(repo, reply_url, html_url, 'patch_id', issue_number, username, True, args)
        elif command == "package-branch" and len(args) >= 1:
            build(repo, reply_url, html_url, 'override_branch_id', args[0], username, True, args[1:])
        elif command == "ping":
            post_comment(reply_url, "pong")
        else:
            unknown_command(reply_url, command)
        break


# Auto build disabled for now until, missing:
# - Ensure buildbot has enough capacity
# - Avoid building the same commit hash multiple times?
# - Cancel previous queued build when pull request was updated?
#
# If we had a fast lint check it could potentially run every time. Also for
# users without commit access if we can make sure the linting script can not
# be modified, at least until there is better isolation on the workers.

def pull_request_event(data: Dict[str, Any]):
    """
    Auto build on pull request updates.
    """
    pass

    # action = data['action']

    # # Only handle potential new commits.
    # if action not in ['opened', 'synchronized']:
    #     return

    # # Ignore not mergeable or already merged.
    # pull_request = data['pull_request']
    # if not pull_request['mergeable']:
    #     return
    # if pull_request['merged']:
    #     return

    # username = pull_request["sender"]["username"]
    # pull_number = str(pull_request['number'])
    # html_url = pull_request['html_url']

    # build(repo, None, html_url, 'patch_id', pull_number, username, False, [])
