# Blender Bot for Gitea

## Commands

Blender developers with commit access can start builds by typing commands in comments on projects.blender.org.

* `@blender-bot build [platform]`: build current pull request. A notification about the build progress / result is shown in the list of commits that make up this pull request.
* `@blender-bot package [platform]`: build current pull request and make package for download. The bot will post the link in a comment on the pull request.
* `@blender-bot build-branch branch [platform]`: build the given branch. The bot will post a link to the build in a comment on the issue / pull request.
* `@blender-bot package-branch branch [platform]`:  build branch and make package for download. The bot will post the link in a comment on the issue / pull request.

`[platform]` is optional, and used to build for just one platform. Possible values:
* `windows` `macos` `linux` (for brevity)
* `windows-amd64` `macos-x86_64` `macos-arm64` `linux-x86_64` (to be specific)

Other:

* `@blender-bot ping`: test if blender-bot is online

## Automation

* When an issue is automatically closed or reopened through a commit, set the
  resolved and confirmed status.
* When an issue is manually closed or reopened, set archived and confirmed or
  needs triage status, unless a valid closed or open status is already set.
* Add git notes to commits to show issues, pull requests and commits that
  reference them.
