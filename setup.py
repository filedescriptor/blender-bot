#!/usr/bin/python3
#
# One time step to clone git repositories.

import config
import utils

for repo in config.GITEA_REPOS:
    repo_dir = config.REPOS_DIR / (repo + ".git")
    if not repo_dir.exists():
        url = config.REPO_BASE_URL + repo + ".git"
        utils.log(f"Cloning {url}")
        repo_dir.parent.mkdir(parents=True, exist_ok=True)
        utils.call(["git", "clone", "--bare", url, repo_dir])
        utils.git_call(["fetch", "origin", "refs/notes/*:refs/notes/*"], repo)
        utils.git_call(["fetch", "origin", "main"], repo)
