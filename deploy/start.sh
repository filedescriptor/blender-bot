#!/bin/sh
# Use only a single worker, concurrent Git repo access not supported yet.
mkdir -p dist/logs
./dist/venv/bin/gunicorn -w 1 'blender_bot:app' -b 127.0.0.1:4000 --daemon --error-logfile dist/logs/error --access-logfile dist/logs/access --capture-output
