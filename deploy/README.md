# Blender Bot for Gitea

### Dependencies

On FreeBSD:

    python39 py39-pip git git-lfs

### Python

Setup isolated Python environment.

    mkdir -p dist
    python3 -m venv dist/venv
    ./dist/venv/bin/pip3 install -r deploy/requirements.txt

### Git

Setup Git user and SSH key for committing and pushing notes to Git repos.

    git config --global user.name "blender-bot"
    git config --global user.email "blender-bot"

    ssh-keygen

### Gitea

Configure Gitea app.ini to be able to access the webhook:

    [webhook]
    ALLOWED_HOST_LIST = 127.0.0.1

Create blender-bot account
* Generate application token with `repo`, `repo_status`, `public_repo`,
  `read:org` scopes enabled. Store it in `dist/secrets/gitea-api-token`.
* Set buildbot password for `blender-bot` acount there in
  `dist/secrets/buildbot-password`.
* Set SSH key in account.
* The account should have ability to edit issues and push to the repositories
  listed in config.py.

In the Blender organization, add a Webhook of type Gitea:

    Target URL: http://127.0.0.1:4000/webhook
    HTTP Method: POST
    Trigger On: All Events

Run one time setup to clone git reposities:

    ./dist/venv/bin/python3 setup.py

## Run

For production:

    ./deploy/start.sh
    ./deploy/stop.sh

For debugging:

    ./deploy/debug.sh
