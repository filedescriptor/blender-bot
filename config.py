from pathlib import Path

# Gitea server.
GITEA_HOST = "10.0.2.226"
GITEA_HTTP_PORT = 3000
GITEA_SSH_PORT = 10022

# Port used when running in debug mode.
DEBUG_WEBHOOK_PORT = 4000

# Folder for location data files.
DIST_DIR = Path(__file__).resolve().parent / "dist"
SECRETS_DIR = DIST_DIR / "secrets"

# Gitea API.
GITEA_API_TOKEN = (SECRETS_DIR / "gitea-api-token").read_text().strip()
GITEA_BASE_URL = f"http://{GITEA_HOST}:{GITEA_HTTP_PORT}"
GITEA_BASE_PUBLIC_URL = "https://projects.blender.org"
GITEA_API_URL = GITEA_BASE_URL + "/api/v1"
GITEA_API_PUBLIC_URL = GITEA_BASE_PUBLIC_URL + "/api/v1"

# Repositories the webhook is used for.
GITEA_REPOS = ["blender/blender",
               "blender/blender-addons",
               "blender/blender-addons-contrib",
               "blender/blender-manual"]

# Buildbot.
BUILDBOT_BASE_URL = "https://builder.blender.org/admin"
BUILDBOT_DOWNLOAD_URL = "https://builder.blender.org/download"
BUILDBOT_USERNAME = "blender-bot"
BUILDBOT_PASSWORD = None

buildbot_auth_path = SECRETS_DIR / "buildbot-password"
if buildbot_auth_path.exists():
    BUILDBOT_PASSWORD = buildbot_auth_path.read_text().strip()

# Local git repositories for notes.
REPOS_DIR = DIST_DIR / "repos"
REPO_BASE_URL = f"ssh://git@{GITEA_HOST}:{GITEA_SSH_PORT}/"

# For replying to commands.
INFO_MESSAGE = "See [documentation](https://projects.blender.org/infrastructure/blender-bot/src/branch/main/README.md) for details."
