#!/usr/bin/python3

import config
import commands
import labels
import notes
import utils

import queue
import signal
import time
import threading
import traceback

from flask import Flask, request, abort

# Background thread to avoid missing events when handling takes a while.
class BackgroundThread(threading.Thread):
    def __init__(self):
        super().__init__()
        self._queue = queue.Queue()
        self._stop_event = threading.Event()
        signal.signal(signal.SIGINT, self.stop)
        signal.signal(signal.SIGTERM, self.stop)

    def stop(self, signal_number, frame):
        self._stop_event.set()
        if self.is_alive():
            self.join()

    def put(self, event, data):
        self._queue.put((event, data))

    def handle_request(self, event, data):
        if event == "issues":
            labels.issues_event(data)
            notes.issues_pulls_event(data)
        elif event == "issue_comment":
            notes.issues_pulls_event(data)
            commands.comment_event(data)
        elif event == "pull_request":
            notes.issues_pulls_event(data)
            commands.pull_request_event(data)
        elif event == "pull_request_comment":
            notes.issues_pulls_event(data)
            commands.comment_event(data)
        elif event == "push":
            notes.push_event(data)

    def run(self):
        print("Background thread started")

        while not self._stop_event.is_set() or not self._queue.empty():
            try:
                event, data = self._queue.get(block=False)
            except queue.Empty:
                time.sleep(1.0)
                continue

            try:
                self.handle_request(event, data)
            except Exception as err:
                traceback.print_exc()
                utils.log("Failed to handle request: " + str(err))

        print("Background thread ended gracefully")


background_thread = BackgroundThread()
background_thread.start()

app = Flask(__name__)


@app.route("/webhook", methods=["POST"])
def root():
    # Get event and data
    event = request.headers.get("X-Gitea-Event", "")
    try:
        data = request.get_json()
    except Exception:
        utils.log("Failed to parse json from request")
        abort(400)

    # Handle in background
    background_thread.put(event, data)

    return "OK"

if __name__ == "__main__":
    app.run(port=config.DEBUG_WEBHOOK_PORT)
